<?php

namespace App\Repository;

class SWAPIRepository
{
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client();
    }

    /**
     * @param string $url
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    private function call(string $url)
    {
        $response = $this->httpClient->request('GET', $url);

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Invalid response');
        }

        if (strpos($response->getHeaderLine('content-type'), 'application/json') === false) {
            throw new \Exception('Not JSON response');
        }

        return$response;
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function getPeoples()
    {
        $url = 'https://swapi.co/api/people';

        do {
            $response = $this->call($url);

            $data = json_decode($response->getBody());

            foreach ($data->results as $person) {
                yield $person;
            }

            $url = $data->next;
        } while ($url);
    }
}
