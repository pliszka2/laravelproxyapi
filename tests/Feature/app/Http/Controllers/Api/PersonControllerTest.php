<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class PersonControllerTest extends TestCase
{
    const CORRECT_NAME = 'Luke Skywalker';
    const INCORRECT_NAME = 'Luke Skywalker2';

    const HEADERS = ['Accept' => 'application/json'];

    private function getAsAuthenticatedUserWithApplicationJson($url)
    {
        $user = factory(User::class)->create();
        return $this->actingAs($user, 'api')->get($url, self::HEADERS);
    }

    /**
     * Test if we could get 200 response code when we send correct person name and authorization token.
     *
     * @return void
     */
    public function testShouldReturn200WhenCorrectDataApplied()
    {
        $response = $this->getAsAuthenticatedUserWithApplicationJson('/api/persons/' . self::CORRECT_NAME);

        $response->assertStatus(200);
    }

    /**
     * Test if we get 404 response code when we send incorrect person name and correct authorization token.
     *
     * @return void
     */
    public function testShouldReturn404WhenCorrectAuthorizationTokenAndPersonNameIncorrect()
    {
        $response = $this->getAsAuthenticatedUserWithApplicationJson('/api/persons/' . self::INCORRECT_NAME);

        $response->assertStatus(404);
    }

    /**
     * Test if we get 401 response code when we send incorrect credentials.
     *
     * @return void
     */
    public function testShouldReturn401WhenIncorrectCredentials()
    {
        $response = $this->get('/api/persons/' . self::CORRECT_NAME, self::HEADERS);

        $response->assertStatus(401);
    }

    /**
     * Test if we could get 200 response code when we send correct person name and authorization token.
     *
     * @return void
     */
    public function testShouldReturnPersonalDetailsWhenCorrectDataApplied()
    {
        $response = $this->getAsAuthenticatedUserWithApplicationJson('/api/persons/' . self::CORRECT_NAME);

        $response->assertJson(
            [
                "name"       => "Luke Skywalker",
                "height"     => "172",
                "mass"       => "77",
                "hair_color" => "blond",
                "skin_color" => "fair",
                "eye_color"  => "blue",
                "birth_year" => "19BBY",
                "gender"     => "male",
                "homeworld"  => "https://swapi.co/api/planets/1/",
                "films"      => [
                    "https://swapi.co/api/films/2/",
                    "https://swapi.co/api/films/6/",
                    "https://swapi.co/api/films/3/",
                    "https://swapi.co/api/films/1/",
                    "https://swapi.co/api/films/7/",
                ],
                "species"    => [
                    "https://swapi.co/api/species/1/",
                ],
                "vehicles"   => [
                    "https://swapi.co/api/vehicles/14/",
                    "https://swapi.co/api/vehicles/30/",
                ],
                "starships"  => [
                    "https://swapi.co/api/starships/12/",
                    "https://swapi.co/api/starships/22/",
                ],
                "created"    => "2014-12-09T13:50:51.644000Z",
                "edited"     => "2014-12-20T21:17:56.891000Z",
                "url"        => "https://swapi.co/api/people/1/",
            ]
        );
    }
}
