<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repository\PersonDetailsRepositoryInterface;

class PersonController extends Controller
{
    protected $personDetailsRepository;
    public function __construct(PersonDetailsRepositoryInterface $personDetailsRepository){
        $this->personDetailsRepository = $personDetailsRepository;
    }

    public function show($name)
    {
        $person = $this->personDetailsRepository->getByName($name);

        if (!$person) {
            abort(404);
        }

        return json_decode($person->data, true);
    }
}
