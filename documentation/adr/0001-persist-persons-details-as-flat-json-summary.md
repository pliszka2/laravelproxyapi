# Persist person details as flat JSON summery

* Status: accepted
* Deciders: Paweł Liszka
* Date: 2019-11-13

## Context and Problem Statement

Technical Story: There is requirement to persist person details from external third part API and then return it to client 

## Decision Drivers

* driver 1, time limitation - there is time limitation task must be made as soon as possible
* driver 2, we only have small amount of data (at most 100 results)
* driver 3, the API could change the response format (it's not look stable)
* driver 4, now wy only need search by name not other fields and probably that won't change in future
* driver 5, we don't know what information are required to retransmit to end user
* driver 6, we always select all data

## Considered Options

* [option 1] - recreate API response structure to database
* [option 2] - use documentary database like MongoDB
* [option 3] - save only part of information 
* [option 4] - save response as JSON to DB

## Decision Outcome

Chosen option: "[option 4]", because: 
* we don't know what information we need,
* we have small amount of data to store,
* we don't need any search mechanism
* we could simply rewrite this later 

### Positive Consequences

* fast implementation
* API independent
* speed up select

### Negative Consequences

* API client must deal with external third part API response changes

