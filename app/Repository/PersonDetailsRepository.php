<?php

namespace App\Repository;

use App\Models\PersonDetails;

class PersonDetailsRepository implements PersonDetailsRepositoryInterface
{
    public function getByName(String $name)
    {
        return PersonDetails::where('name', $name)->first();
    }
}
