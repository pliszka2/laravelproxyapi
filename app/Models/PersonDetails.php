<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonDetails extends Model
{
    public $timestamps = false;
}
