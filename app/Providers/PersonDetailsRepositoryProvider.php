<?php

namespace App\Providers;

use App\Repository\PersonDetailsRepository;
use Illuminate\Support\ServiceProvider;


class PersonDetailsRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repository\PersonDetailsRepositoryInterface", function () {
            return new PersonDetailsRepository();
        });
    }
}
