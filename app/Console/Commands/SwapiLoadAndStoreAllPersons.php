<?php

namespace App\Console\Commands;

use App\Models\PersonDetails;
use App\Repository\SWAPIRepository;
use Illuminate\Console\Command;

class SwapiLoadAndStoreAllPersons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'swapi:persons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        $people = (new SWAPIRepository())->getPeoples();

        foreach ($people as $person) {
            $model = new PersonDetails();
            $model->name = $person->name;
            $model->data = json_encode($person);
            $model->save();
        }
    }
}
