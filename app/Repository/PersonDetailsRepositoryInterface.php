<?php

namespace App\Repository;

interface PersonDetailsRepositoryInterface
{
    public function getByName(String $name);
}
