# Person name as primary key

* Status: accepted
* Deciders: Paweł Liszka
* Date: 2019-11-13

## Context and Problem Statement

Technical Story: There is requirement to identify person by it's name 

## Decision Drivers

* driver 1, database agnostic

## Considered Options

* [option 1] - auto increment
* [option 2] - GUID
* [option 3] - name

## Decision Outcome

Chosen option: "[option 3]", because: 
* we identify person by name so this field must be unique

### Positive Consequences

* we don't need extra GUID field
* we are independent (we do not need any auto increment value from DB)

### Negative Consequences

* name must by unique 

